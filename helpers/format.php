<?php 
	/**
	*  Format Class
	*/
	class Format
	{
		
		public function formatDate( $date )
		{
			return date('d-m-Y H:i:s', strtotime($date));
		}

		public function textShorten($text, $textLimit = 400)
		{
			$text = $text. " ";
			$text = substr($text, 0, $textLimit);
			$text = substr($text, 0, strrpos($text, ' '));
			$text = $text. "...";
			return $text;
		}

		public function validation( $data ){
			$data = trim( $data );
			$data = stripcslashes( $data );
			$data = htmlspecialchars( $data );
			return $data;
		}
	}
?>