<?php include 'inc/header.php' ?>
<?php 
	if (!isset($_GET['id']) || $_GET['id'] == NULL) {
		header('Location: 404.php');
	}else{
?>
	<div class="contentsection contemplete clear">
		<div class="maincontent clear">
			<div class="about">
				<?php 
					$post_id = $_GET['id'];
					$query = "SELECT * FROM tbl_post WHERE id = $post_id";
					$post = $db->select($query);
					if ($post) {
						while ( $result = $post->fetch_assoc() ) :
				?>
				<h2><?php echo $result['title']; ?></h2>
				<h4><?php echo $fm->formatDate($result['date']); ?>, By <a href="#"><?php echo $result['author'] ?></a></h4>
				<img src="admin/upload/<?php echo $result['image']; ?>" alt="MyImage"/>
				<?php echo $result['body']; ?>
				<div class="relatedpost clear">
					<h2>Related articles</h2>
					<?php
						$cat_id = $result['cat'];
						$query = "SELECT * FROM tbl_post WHERE id = $post_id";
						$post = $db->select($query);
						if ($post) {
							while ( $result = $post->fetch_assoc() ) :
					?>
					<a href="post.php?id=<?php echo $result['id']; ?>"><img src="admin/upload/<?php echo $result['image']; ?>" alt="post image"/></a>
					<?php endwhile; }else{
						echo 'Related articles not found';
					} ?>
				</div>
				<?php endwhile; // end loop ?>
				<?php }else{
						header('Location: 404.php');
					}
				?>
			</div>
		</div>
		<?php include 'inc/sidebar.php'; ?>
	</div>
	<?php } ?>

	<div class="footersection templete clear">
	  <div class="footermenu clear">
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">About</a></li>
			<li><a href="#">Contact</a></li>
			<li><a href="#">Privacy</a></li>
		</ul>
	  </div>
	  <p>&copy; Copyright Training with live project.</p>
	</div>
</body>
</html>