<?php include 'inc/header.php' ?>
<?php include 'inc/slider.php' ?>

	<div class="contentsection contemplete clear">
		<div class="maincontent clear">
			<?php 
				if ( !isset($_GET['search']) || $_GET['search'] == NULL ) {
					header( "Location: 404.php" );
			?>
			<?php 
				} else{
					$search = $_GET['search'];
					$query = "SELECT * FROM tbl_post WHERE title 
													LIKE '%$search%' 
													OR body LIKE '%$search%'";
					$posts = $db->select( $query );
					if ( $posts ){
						while ( $p = $posts->fetch_assoc() ) :
			?>
					<div class="samepost clear">
						<h2><a href="post.php?id=<?php echo $p['id']; ?>"><?php echo $p['title']; ?></a></h2>
						<h4><?php echo $fm->formatDate($p['date']); ?>, By <a href="#"><?php echo $p['author'] ?></a></h4>
						<?php if ( $p['image'] ) : ?>
							<a href="#">
								<img src="admin/upload/<?php echo $p['image']; ?>" alt="post image" title="<?php echo $p['title']; ?>"/>
							</a>
						<?php else : ?>
							<a href="#">
								<img src="admin/upload/no_image.png" alt="post image"/>
							</a>
						<?php endif; ?>
						<p>
							<?php echo $fm->textShorten($p['body'], 200); ?>
						</p>
						<div class="readmore clear">
							<a href="post.php?id=<?php echo $p['id']; ?>">Read More</a>
						</div>
					</div>
					<?php endwhile; ?>
			<?php 
					}else{
						echo "<h2> Không có bài viết! </h2>";
					}
			?>
			<?php } ?>

		</div>

		<?php include 'inc/sidebar.php' ?>

	</div>

<?php include 'inc/footer.php' ?>