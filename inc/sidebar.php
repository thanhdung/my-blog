<div class="sidebar clear">
	<div class="samesidebar clear">
		<h2>Categories</h2>
			<ul>
				<?php 
					$query = "SELECT * FROM `tbl_category`";
					$category = $db->select($query);
					if ( $category ) {
						while ( $result = $category->fetch_assoc() ) :
				?>
					<li><a href="posts.php?category=<?php echo $result['id']; ?>"><?php echo $result['name']; ?></a></li>
				<?php endwhile; }else{ ?>
					<li><a href="#">No Category Created</a></li>
				<?php } ?>				
			</ul>
	</div>
	
	<div class="samesidebar clear">
		<h2>Latest articles</h2>
			<?php 
				$query = "SELECT * FROM `tbl_post` LIMIT 5";
				$posts = $db->select( $query );
				if ( $posts ) :
					while( $post = $posts->fetch_assoc() ) :
			?> 
			<div class="popular clear">
				<h3><a href="post.php?id=<?php echo $post['id']; ?>"><?php echo $post['title']; ?></a></h3>
				<a href="post.php?id=<?php echo $post['id']; ?>">
					<img src="admin/upload/<?php echo $post['image']; ?>" alt="post image"/>
				</a>
				<?php echo $fm->textShorten($post['body'], 120); ?>
			</div>
			<?php endwhile; endif; ?>
	</div>
	
</div>