﻿<?php include 'inc/header.php'; ?>

<?php include 'inc/sidebar.php'; ?>

<div class="grid_10">
	<div class="box round first grid">
	    <h2>Post List</h2>
	    <div class="block">  
	        <table class="data display datatable" id="example">
			<thead>
				<tr>
					<th>Post Title</th>
					<th>Description</th>
					<th>Category</th>
					<th>Image</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$query = "SELECT * FROM `tbl_post`";
					$post = $db->select( $query );
					if ( $post ) :
						while ( $p = $post->fetch_assoc() ) :
				?>
				<tr class="odd gradeX">
					<td><?php echo $p['title']; ?></td>
					<td><?php //echo $p['body']; ?></td>
					<td><?php echo $p['cat']; ?></td>
					<td class="center">
						<img src="..upload/<?php echo $p['image']; ?>" title="<?php echo $p['title']; ?>">
					</td>
					<td><a href="">Edit</a> || <a href="">Delete</a></td>
				</tr>
				<?php endwhile; ?>
				<?php else : ?>
					<h2>Không có bài viết nào.</h2>
				<?php endif; ?>
			</tbody>
		</table>

	   </div>
	</div>
</div>

<?php include 'inc/footer.php'; ?>
        
