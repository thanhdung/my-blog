﻿<?php include 'inc/header.php'; ?>
        
<?php include 'inc/sidebar.php'; ?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Category List</h2>
        <div class="block">        
            <table class="data display datatable" id="example">
			<thead>
				<tr>
					<th>Serial No.</th>
					<th>Category Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$query = "SELECT * FROM `tbl_category`";
					$cat = $db->select( $query );
					if ( $cat ) :
						while ( $c = $cat->fetch_assoc() ) :
				?>
				<tr class="odd gradeX">
					<td><?php echo $c['id']; ?></td>
					<td><?php echo $c['name']; ?></td>
					<td><a href="">Edit</a> || <a href="">Delete</a></td>
				</tr>
				<?php endwhile; ?>
				<?php else: ?>
				<h2>Không có danh mục.</h2>
				<?php endif; ?>
			</tbody>
		</table>
       </div>
    </div>
</div>

<?php include 'inc/footer.php'; ?>
